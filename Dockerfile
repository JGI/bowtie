FROM debian:jessie

ENV BOWTIE_VERSION=1.1.2

RUN set -ex \
&&  apt-get update -y \
&&  apt-get install -y \
    bash \
    ca-certificates \
    python \
    wget \
    zip

RUN mkdir /bowtie/ 

# downloading bowtie...
RUN cd /bowtie/ \
&& wget --no-check-certificate https://sourceforge.net/projects/bowtie-bio/files/bowtie/${BOWTIE_VERSION}/bowtie-${BOWTIE_VERSION}-linux-x86_64.zip

# unpacking bowtie...
RUN cd /bowtie/ \
&& unzip bowtie-${BOWTIE_VERSION}-linux-x86_64.zip

ENV PATH=/bowtie/bowtie-${BOWTIE_VERSION}/:$PATH

CMD ["/bin/bash"]


